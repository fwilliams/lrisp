all: program

# Replace program with OpenCV program.
program: ./src/C/program.c
	gcc ./src/C/program.c `pkg-config --cflags opencv` `pkg-config --libs opencv` -o ./exec/program.o

threshold: ./src/C/threshold.c
	gcc ./src/C/threshold.c `pkg-config --cflags opencv` `pkg-config --libs opencv` -o ./exec/threshold.o

canny: ./src/C/canny.c
	gcc ./src/C/canny.c `pkg-config --cflags opencv` `pkg-config --libs opencv` -o ./exec/canny.o