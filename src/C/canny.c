#include <cv.h>
#include <highgui.h>
#include <stdio.h>

int main(int argc, char** argv) {
  cvNamedWindow( "red", 1 );
  //cvNamedWindow( "canny", 1 );
  cvNamedWindow( "color", 1 );

  IplImage* fullColorImg = cvLoadImage( argv[1], CV_LOAD_IMAGE_COLOR );
  IplImage* redChanImg = cvCreateImage( cvGetSize(fullColorImg), IPL_DEPTH_8U, 1 );
  IplImage* enhanced = cvCreateImage( cvGetSize(fullColorImg), IPL_DEPTH_8U, 1 );
  IplImage* cannyRed = cvCreateImage( cvGetSize(fullColorImg), IPL_DEPTH_8U, 1 );

  cvSplit( fullColorImg, NULL, NULL, redChanImg, NULL );\
  
  cvThreshold(redChanImg, enhanced, 200, 255, CV_THRESH_TOZERO);

  cvCanny(enhanced, cannyRed, 10, 100, 3);

  cvShowImage( "red", enhanced );
  cvShowImage( "canny", cannyRed );
  cvShowImage( "color", fullColorImg );

  while( 1 ) {
    if( ( cvWaitKey(10)&0x7f ) == 27 )  {
      break;
    }
  }

  cvDestroyWindow( "color" );
  cvDestroyWindow( "red" );
  cvDestroyWindow( "canny" );

  cvReleaseImage( &fullColorImg );
  cvReleaseImage( &redChanImg );
  cvReleaseImage( &enhanced );
}
