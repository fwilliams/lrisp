#include "highgui.h"

int main(int argc, char **argv) {
  IplImage* img = cvLoadImage(argv[1], CV_LOAD_IMAGE_COLOR);
  cvNamedWindow("Example OpenCV program", CV_WINDOW_AUTOSIZE);
  cvShowImage("Example OpenCV program", img);
  cvWaitKey(0);
  cvReleaseImage(&img);
  cvDestroyWindow("Example OpenCV program");
}

