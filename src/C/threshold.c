#include <cv.h>
#include <highgui.h>

int main(int argc, char** argv) {
  IplImage* fullColorImg = cvLoadImage( argv[1], CV_LOAD_IMAGE_COLOR );
  IplImage* redChanImg = cvCreateImage( cvGetSize(fullColorImg), IPL_DEPTH_8U, 1 );
  IplImage* blueChanImg = cvCreateImage( cvGetSize(fullColorImg), IPL_DEPTH_8U, 1 );
  IplImage* greenChanImg = cvCreateImage( cvGetSize(fullColorImg), IPL_DEPTH_8U, 1 );

  IplImage* enhanced = cvCreateImage( cvGetSize(fullColorImg), IPL_DEPTH_8U, 1 );
  IplImage* enhanced_green = cvCreateImage( cvGetSize(fullColorImg), IPL_DEPTH_8U, 1 );
  IplImage* enhanced_blue = cvCreateImage( cvGetSize(fullColorImg), IPL_DEPTH_8U, 1 );
  

  IplImage* cannyRed = cvCreateImage(cvGetSize(enhanced), enhanced->depth, 1);

  cvSplit( fullColorImg, blueChanImg, greenChanImg, redChanImg, NULL );

  if( argv[2] == "enhance" ) {
    cvDilate( redChanImg, enhanced, NULL, 1 );
  }

  cvThreshold(redChanImg, enhanced, 50, 255, CV_THRESH_TOZERO);
  cvThreshold(greenChanImg, enhanced_green, 50, 255, CV_THRESH_TOZERO);
  cvThreshold(blueChanImg, enhanced_blue, 50, 255, CV_THRESH_TOZERO);

  cvCanny(enhanced, cannyRed, 10, 100, 3);
  
  cvNamedWindow( "red", 1 );
  cvNamedWindow( "green", 1 );
  cvNamedWindow( "blue", 1 );
  cvNamedWindow( "color", 1 );

  cvShowImage( "red", enhanced);
  cvShowImage( "canny", cannyRed);
  //cvShowImage( "green", enhanced_green);
  //cvShowImage( "blue", enhanced_blue);
  cvShowImage( "color", fullColorImg );

  while( 1 ) {
    if( ( cvWaitKey(10)&0x7f ) == 27 )  {
      break;
    }
  }

  cvDestroyWindow( "color" );
  cvDestroyWindow( "red" );

  cvReleaseImage( &fullColorImg );
  cvReleaseImage( &redChanImg );
  cvReleaseImage( &enhanced );
}
