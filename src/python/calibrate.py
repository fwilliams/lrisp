import cv, os

# Board width and height
BOARD_W = 9
BOARD_H = 6

# Number of board images
N_BOARDS = 13

# Number of points to collect per board
BOARD_N = BOARD_W * BOARD_H

BOARD_SZ = (BOARD_W, BOARD_H)

# Collection of points found in the images
image_points = cv.CreateMat(N_BOARDS*BOARD_N, 2, cv.CV_32FC1)
object_points = cv.CreateMat(N_BOARDS*BOARD_N, 3, cv.CV_32FC1)
point_counts = cv.CreateMat(N_BOARDS, 1, cv.CV_32SC1)

# Outputs
intrinsics_matrix = cv.CreateMat(3, 3, cv.CV_32FC1)
distortion_coeff = cv.CreateMat(5, 1, cv.CV_32FC1)
rvecs = cv.CreateMat(N_BOARDS, 9, cv.CV_32FC1)
tvecs = cv.CreateMat(N_BOARDS, 3, cv.CV_32FC1)


def resize_and_show_in_window(name, image, scale):
	resized = cv.CreateMat(image.rows / scale, image.cols / scale, cv.CV_8UC3)
	cv.Resize(image, resized)
	save_dir = '../../img/test_images/res/'
	cv.SaveImage(save_dir + name + '-shown.jpg', image)
	cv.NamedWindow(name, 1)
	cv.ShowImage(name, resized)

def find_chessboard_and_subpix(image, show_test_points=False):
	gray_image = cv.CreateImage(cv.GetSize(image), 8, 1)

	found_all, corners = cv.FindChessboardCorners(image, BOARD_SZ)
	
	cv.CvtColor(image, gray_image, cv.CV_BGR2GRAY)
	cv.FindCornerSubPix(gray_image, corners, (11, 11), (-1, -1), (cv.CV_TERMCRIT_EPS | cv.CV_TERMCRIT_ITER, 30, 0.1))

	if show_test_points:
		cv.DrawChessboardCorners(image, BOARD_SZ, corners, found_all)
		
	return corners, found_all


def update_data(iteration, image, corners):
	for i in range(BOARD_N):
		offset = i+(iteration*BOARD_N)
		image_points[offset, 0] =  corners[i][0]
		image_points[offset, 1] = corners[i][1]
		object_points[offset, 0] = i/BOARD_W
		object_points[offset, 1] = i%BOARD_W
		object_points[offset, 2] = 0.0
		point_counts[iteration, 0] = len(corners)


def cvmat_to_list(mat):
	lst = []
	for i in range(mat.rows):
		lst.append([])
		for j in range(mat.cols):
			lst[i].append(mat[i, j])
	return lst


def main():
	cv.Set(intrinsics_matrix, 0.0)
	cv.Set(distortion_coeff, 0.0)

	files = os.listdir('/home/francis/Documents/code/lrisp/img/calibration/gumstix-camera')
	failed = []
	for i in range(N_BOARDS):
		print 'Processing board %d' %(i+1)
		
		image = cv.LoadImageM('/home/francis/Documents/code/lrisp/img/calibration/gumstix-camera/' + files[i])

		corners, found_all = find_chessboard_and_subpix(image, False)

		if found_all:
			update_data(i, image, corners)
		else:
			print 'Failed to find all points for board %d:' %(i+1)
			failed.append(files[i])

	print failed, '\n'

	intrinsics_matrix[0, 0] = 1.0
	intrinsics_matrix[1, 1] = 1.0

	cv.CalibrateCamera2(object_points, image_points, 
					    point_counts, cv.GetSize(image),
					    intrinsics_matrix, distortion_coeff,
				    	rvecs, tvecs)

	print '\nIntrinsics Matrix'
	print cvmat_to_list(intrinsics_matrix)
	print '\nDistorion Vector'
	print cvmat_to_list(distortion_coeff)

	in_image = cv.LoadImageM('/home/francis/Documents/code/lrisp/img/calibration/gumstix-camera/2010-11-16-014503.jpg')
	out_image = resized = cv.CreateMat(in_image.rows, in_image.cols, cv.CV_8UC3)

	cv.Undistort2(in_image, out_image, intrinsics_matrix, distortion_coeff)

	resize_and_show_in_window('distorted', in_image, 1)
	resize_and_show_in_window('undistorted', out_image, 1)

	# Wait for escape key
	while True:
		if cv.WaitKey(10)&0x7f == 27:
			break

if __name__ == '__main__':
	main()

