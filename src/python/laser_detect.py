import cv, sys, os

MORPH_ITERATIONS = 1
THRESHOLD = 100
TEMPLATE_DIR = '../../img/test_images/templates/'
RESULT_DIR = '../../img/test_images/res/'

def list_to_cv_mat(lst):
	mat = cv.CreateMat(len(lst), len(lst[0]), cv.CV_32FC1)
	for i in range(len(lst)):
		for j in range(len(lst[0])):
			mat[i, j] = lst[i][j]
	return mat

INTRISICS = list_to_cv_mat([[622.6984252929688, 0.0, 354.41357421875], [0.0, 619.541259765625, 281.2809753417969], [0.0, 0.0, 1.0]])
DISTORTION = list_to_cv_mat([[-0.41915765404701233], [0.24233604967594147], [0.0006734276539646089], [-0.000359527301043272], [-0.09664762020111084]])


def main():
	image = cv.LoadImage(sys.argv[1])
	undistorted = undistort_image(image)

	red_chan, green_chan, blue_chan = split_image(undistorted, False)
	smooth = smooth_image(green_chan)
	thresh = threshold_image(smooth)
	opened = morph_open(thresh, MORPH_ITERATIONS)
	contour_matched, centers = match_by_contour(opened, True, True)
	

	image2 = cv.LoadImage(sys.argv[2])
	undistorted2 = undistort_image(image2)
	red_chan2, green_chan2, blue_chan2 = split_image(undistorted2, False)
	smooth2 = smooth_image(green_chan2)
	thresh2 = threshold_image(smooth2)
	opened2 = morph_open(thresh2, MORPH_ITERATIONS)
	contour_matched2, centers2 = match_by_contour(opened2, True, True)

	show = convert_to_color(opened)

	draw_center_points(undistorted, centers2, 2, cv.Scalar(0, 0, 255))
	
	draw_center_points(undistorted2, centers2, 2, cv.Scalar(0, 0, 255))



	show_in_new_window('original', undistorted2, 1., True)
	#show_in_new_window('undistorted', undistorted, 1., True)
	#show_in_new_window('green channel', green_chan, 1., True)
	#show_in_new_window('threshold', thresh, 1.)
	show_in_new_window('opened', undistorted, 1.)
	#show_in_new_window('contour_match', contour_matched, 1., True)
	#show_in_new_window('template_match', template_matched, 0.2, True)	

def convert_to_color(image):
	dst = cv.CreateImage(cv.GetSize(image), image.depth, 3)
	cv.CvtColor(image, dst, cv.CV_GRAY2BGR)
	return dst

def undistort_image(image):
	undistorted = cv.CreateImage(cv.GetSize(image), image.depth, image.channels)
	cv.Undistort2(image, undistorted, INTRISICS, DISTORTION)
	return undistorted

def split_image(image, showSplit=False, splitScale=0.2):
	red_chan = cv.CreateImage(cv.GetSize(image), cv.IPL_DEPTH_8U, 1)
	blue_chan = cv.CreateImage(cv.GetSize(image), cv.IPL_DEPTH_8U, 1)
	green_chan = cv.CreateImage(cv.GetSize(image), cv.IPL_DEPTH_8U, 1)

	cv.Split(image, red_chan, blue_chan, green_chan, None)

	if showSplit:
		show_in_new_window('loaded image', image, 0.2, True)
		show_in_new_window('red image', red_chan, 0.2, True)
		show_in_new_window('green image', green_chan, 0.2, True)
		show_in_new_window('blue_image', green_chan, 0.2, True)

	return red_chan, blue_chan, green_chan


def get_comparison(filename):
	img = cv.LoadImage(TEMPLATE_DIR + filename)
	r, g, b = split_image(img, False)
	s = smooth_image(g)
	t = threshold_image(s)
	o = morph_open(t, MORPH_ITERATIONS)
	s = cv.CreateMemStorage(0)
	contour = cv.FindContours(o, s)
	return contour


# Need to adjust thresholding technique based on image
# TODO: Implement this algorithm
def threshold_image(image):
	dst = cv.CreateImage(cv.GetSize(image), cv.IPL_DEPTH_8U, 1)
	cv.Threshold(image, dst, THRESHOLD, 255, cv.CV_THRESH_BINARY)
	return dst

def smooth_image(image):
	dst = cv.CreateImage(cv.GetSize(image), image.depth, image.channels)
	cv.Smooth(image, dst, 3, 3)
	return dst


def morph_open(image, iterations):
	struct_el = cv.CreateStructuringElementEx(3, 3, 1, 1, cv.CV_SHAPE_ELLIPSE)
	dst = cv.CreateImage(cv.GetSize(image), cv.IPL_DEPTH_8U, 1)
	cv.MorphologyEx(image, dst, None, struct_el, cv.CV_MOP_OPEN, iterations)
	return dst


def match_by_template(src, template):
	template = morph_open(threshold_image(cv.LoadImage(template, 0)), MORPH_ITERATIONS)
	res_size = (src.width-template.width+1), (src.height-template.height+1)
	res = cv.CreateImage(res_size, cv.IPL_DEPTH_32F, 1)
	cv.MatchTemplate(src, template, res, cv.CV_TM_SQDIFF)
	return res


def match_by_contour(src, draw_countours=False, return_centers=False):
	image = cv.CreateImage(cv.GetSize(src), 8, 1)
	cv.Copy(src, image)
	storage = cv.CreateMemStorage(0)

	contours = cv.FindContours(image, storage)

	# Get different comparators
	comparators = []
	cmp_files = os.listdir(TEMPLATE_DIR)
	for i in range(len(cmp_files)):
		comparators.append(get_comparison(cmp_files[i]))
	c = contours
	lasers = []

	while c != None:
		for comp in comparators:
			diff = cv.MatchShapes(comp, c, cv.CV_CONTOURS_MATCH_I1)
			if diff < 0.3:
				lasers.append(c)
		c = c.h_next()

	detected_points = []
	centers = []
	for l in lasers:
		box = cv.BoundingRect(l)
		center = (box[0]+box[2]/2, box[1]-box[3]/2)
		detected_points.append(center)

	if draw_countours:
		img = cv.CreateImage(cv.GetSize(src), 8, 3)
		cv.CvtColor(src, img, cv.CV_GRAY2BGR)
		for l in lasers:
			box = cv.BoundingRect(l)
			pt1 = (box[0], box[1])
			pt2 = (box[0] + box[2], box[1] + box[3])
			centers.append((box[0]+box[2]/2, box[1]+box[3]/2))
			cv.Rectangle(img, pt1, pt2, cv.Scalar(255, 0, 0), 5)
		if return_centers:
			return img, centers
		return img


def draw_center_points(image, centers, radius=3, color=cv.Scalar(255, 0, 0)):
	for pt in centers:
		cv.Circle(image, pt, radius, color, -1)


def show_in_new_window(name, image, scale=1, save=False, save_dir=RESULT_DIR):
	if scale != 1:
		scaled = cv.CreateImage((int(image.width*scale), int(image.height*scale)), image.depth, image.channels)
		cv.Resize(image, scaled)
	else:
		scaled = image

	if save:
		cv.SaveImage(save_dir + name + '.jpg', image)

	cv.NamedWindow(name, 1)
	cv.ShowImage(name, scaled)


def wait_for_esc():
	while True:
		if cv.WaitKey(10)&0x7f == 27:
			break


if __name__ == '__main__':
	main()
	wait_for_esc()																																												